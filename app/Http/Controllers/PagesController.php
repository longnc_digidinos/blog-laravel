<?php
/**
 * Created by PhpStorm.
 * User: LongNC
 * Date: 12/27/2018
 * Time: 10:27 AM
 */
namespace app\Http\Controllers;

use Illuminate\Routing\Controller;
use App\Post;

class PagesController extends Controller {
    public function getIndex() {
        # process variable data or params
        # talk to the model
        # receive from the model
        # compile or process data from the model if needed
        # pass that data to the correct view
        $posts = Post::orderBy('created_at', 'desc')->limit(4)->get();
        return view("pages.welcome")->withPosts($posts);
    }

    public function getAbout() {
        #return "About Me";
        return view("pages.about");
    }

    public function getContact() {
        #return "Hello Contact Page";
        return view("pages.contact");
    }
}